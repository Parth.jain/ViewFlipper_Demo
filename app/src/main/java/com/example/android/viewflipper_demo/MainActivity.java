package com.example.android.viewflipper_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ViewFlipper;

public class MainActivity extends AppCompatActivity {

    ViewFlipper simpleFlipper;
    Button btnViewSwitcher,btnImageSwitcher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnViewSwitcher =findViewById(R.id.buttonViewSwitcher);
        btnImageSwitcher=findViewById(R.id.gotoImageSwitcher);
        simpleFlipper=findViewById(R.id.simpleViewFlipper);

        Animation in=AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out=AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);

        simpleFlipper.setInAnimation(in);
        simpleFlipper.setOutAnimation(out);
        simpleFlipper.setFlipInterval(3000);
        simpleFlipper.setAutoStart(true);

        btnViewSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ViewSwitcherActivity.class));
            }
        });

        btnImageSwitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ImageSwitcherActivity.class));

            }
        });
    }
}
