package com.example.android.viewflipper_demo;

import android.animation.AnimatorListenerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;

public class ImageSwitcherActivity extends AppCompatActivity {
    ImageSwitcher imageSwitcher;
    Button btnImageSwitch;

    int imageIds[]={R.drawable.image,R.drawable.image2,R.drawable.image3,R.drawable.image4,R
            .drawable.image5};
    int counts=imageIds.length;
    int currentIndex=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_switcher);

        imageSwitcher=(ImageSwitcher)findViewById(R.id.simpleImageSwitcher);
        btnImageSwitch=findViewById(R.id.buttonImageSwitch);
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView=new ImageView(ImageSwitcherActivity.this);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout
                        .LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
                return imageView;
            }
        });

        Animation in= AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out= AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);

        imageSwitcher.setInAnimation(in);
        imageSwitcher.setOutAnimation(out);

        btnImageSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentIndex++;
                if(currentIndex==counts){
                    currentIndex=0;
                }
                    imageSwitcher.setImageResource(imageIds[currentIndex]);

            }
        });

    }
}
