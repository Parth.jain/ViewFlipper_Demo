package com.example.android.viewflipper_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ViewSwitcher;

public class ViewSwitcherActivity extends AppCompatActivity {

    ViewSwitcher viewSwitcher;
    Button btnSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_switcher);

        btnSwitch=findViewById(R.id.buttonSwitch);
        viewSwitcher=findViewById(R.id.simpleViewSwitcher);

        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out=AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);

        viewSwitcher.setInAnimation(in);
        viewSwitcher.setOutAnimation(out);

        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewSwitcher.showNext();
            }
        });
    }
}
